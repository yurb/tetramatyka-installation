/*
 * Init OSC
 */
var osc = new OSC();
osc.open();

/*
 * Setup D3
 */
var w = 960, h = 600;

var secPerDay = 24 * 60 * 60;
var startDay = new Date(2017, 9, 9);
var endDay = new Date(2017, 9, 23);
var days = (endDay - startDay) / 1000 / secPerDay;

var timeScale = d3.scaleLinear()
    .domain([0, secPerDay]) // Seconds in a day
    .range([h, 0]); // Bottom up

var daysArray = new Array();
for (var i = 0; i < days; i++) { step = w / days; daysArray.push(i * step); }

var dayScale = d3.scaleTime()
    .domain([startDay, endDay])
    .range([0, w]);

var dayScalePos = d3.scaleQuantize()
    .domain([0, w])
    .range(daysArray);

//var freqScale = d3.scaleSequential(val => { var col = val * 175; return d3.rgb(col, col, col) })
var freqScale = d3.scaleSequential(d3.interpolateRainbow)
    .domain([0.32, 0.4]);

var gainScale = d3.scaleLinear()
    .domain([0, 1])
    .range([0, w / days])
    .clamp(true);

axisTop = d3.axisBottom(dayScale)
    .ticks();

axisTopElement = d3.select("#axis-top")
    .attr("width", w + 100)
    .attr("height", 50)
    .append("g")
    .attr("transform", "translate(50 0)");

axisTopElement.call(axisTop)

graph = d3.select("#graph")
    .attr("width", w)
    .attr("height", h)
    .attr("margin-left", "50px");

ctx = graph.node().getContext("2d");

/*
 * Plotting
 */
function drawRow (time, gain, brightness) {
    var x = dayScalePos(dayScale(new Date((time - (60 * 60 * 3)) * 1000))),
        y = timeScale(time % secPerDay),
        width = gainScale(gain),
        height = 1;
    ctx.fillStyle = freqScale(brightness).toString();
    ctx.fillRect(x, y, width, height);
}

function line (msg) {
    var time = msg.args[0];
    var gain = msg.args[1];
    var brightness = msg.args[2];
    var offset = (Date.now() / 1000) - time;
    console.log(`time: ${time}, offset: ${offset}, gain: ${gain}, brightness: ${brightness}`);
    drawRow(time, gain, brightness);
}

osc.on('/graph', line);

/* 
 * Load historical data on boot
 */
d3.request("data.log")
    .mimeType("text/csv")
    .response((xhr) => d3.csvParseRows(xhr.responseText))
    .get((err, data) => {
        data.forEach((row) => {
            drawRow(row[0], row[1], row[2]);
        });
});


/*
 * Handle drawings.
 * Maintain a poll of 10 drawings, rotate it.
 */
d3.request("drawings/list.txt")
    .mimeType("text/txt")
    .response((xhr) => xhr.responseText.split("\n"))
    .get((err, list) => {
        drawingFiles = list;
    });

function newDrawing() {
    var numDrawings = drawingFiles.length;
    var which = Math.floor(Math.random() * (numDrawings + 1));
    var x = Math.floor(Math.random() * 600);
    var y = Math.floor(Math.random() * 300);
    var angle = Math.floor(Math.random() * 180);
    var drawing = $("<img>")
        .addClass("drawing")
        .attr("src", "drawings/" + drawingFiles[which])
        .css("top", y)
        .css("left", x)
        .css("transform", `rotate(${angle}deg)`);
    return drawing;
}

function rotateDrawings() {
    var drawings = $("#drawings");
    if (drawings.children('.drawing').length >= 10) {
        drawings.children('.drawing').first().remove();
    }
    drawings.append(newDrawing());
}

osc.on('/impulse', rotateDrawings);
