/*
 * Bridge between WebSocket and UDP OSC.
 */
const OSC = require('osc-js');

// Forward to localhost:57120
// (the default SuperCollider lang OSC port)
const config = { udpClient: { port: 57120 } }

const osc = new OSC({ plugin: new OSC.BridgePlugin(config) });
osc.open();
